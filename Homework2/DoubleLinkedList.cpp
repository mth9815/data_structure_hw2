//Author: Sijia Zhang, Ziyi Wang
//create time: 10/29/2017

#include "DoubleLinkedList.h"
#include "DNode.h"
#include "ListIterator.h"
#include <iostream>

//destructor.
template<typename T>
DoublyLinkedList<T>::~DoublyLinkedList() {
    DNode<T>* curr = root;
    for (int i = 0; i < length; i++)
    {
        DNode<T>* next = curr->get_next();
        std::cout << "deleting" << curr->get_value() << std::endl;
        delete curr;
        curr = next;
    }
}

//Add
template <typename T>
void DoublyLinkedList<T>::Add(T val) {
    //create a place for the new node.
    DNode<T>* Node_new = new DNode<T>(val);

    //If the root is NULL, then directly set Node_new as root.
    if (root == nullptr) {
        root = Node_new;
    }
        //else, connect Node_new to the end of original linked list.
    else {
        DNode <T>* it = root;
        while (it->get_next() != nullptr) {
            it = it->get_next(); //until the end of the list.
        }
        it->set_next(Node_new);
        Node_new->set_prev(it);
    }
    //add 1 to length.
    length += 1;
}


//Insert
template <typename T>
void DoublyLinkedList<T>::Insert(T val, int index) {
    //create a place for the new Node.
    DNode<T>* new_node = new DNode<T>(val);

    //If the index is larger than length, then error occurs.
    if (index > length || index < 0) {
        std::cout << "out of scope." << std::endl;
        return;
    }

    if (index == 0)
    {
        new_node->set_next(root);
        root->set_prev(new_node);
        root = new_node;
    }
    else
    {
        DNode<T>* curr = root;
        for (int i = 0; i < index - 1; i++)
        {
            curr = curr->get_next();
        }
        new_node->set_prev(curr);
        new_node->set_next(curr->get_next());
        curr->get_next()->set_prev(new_node);
        curr->set_next(new_node);
    }
    // update length
    length++;
}

//Get
template <typename T>
T DoublyLinkedList<T>::Get(int index) {
    //If index is larger than length, then error occurs.
    if (index >= length || index < 0) {
        std::cout << "out of scope!" << std::endl;
        return T();
    }
    DNode <T>* it = root;
    for (int i = 0; i < index; i++)
    {
        it = it->get_next();
    }
    //return the value of given spot.
    return it->get_value();
}

//IndexOf
template <typename T>
int DoublyLinkedList<T>::IndexOf(T val) {
    //if this is an empty list, return -1 to show doesn't find
    if (root == nullptr) {
        std::cout << "doesn't exist!" << std::endl;
        return -1;
    }
    DNode <T>* it = root;
    for (int i = 0; i < length; i++)
    {
        if (it->get_value() == val)
            return i;
        it = it->get_next();
    }
    return -1;
}


//GetIterator
template<typename T>
ListIterator<T> DoublyLinkedList<T>::GetIterator()
{
    return ListIterator<T>(root);
}

//Remove
template <typename T>
T DoublyLinkedList<T>::Remove(int index) {
    // check index validation
    if (index < 0 || index >= length)
    {
        std::cout << "index out of range for remove" << std::endl;
        return T();
    }

    T val;    // value of the deleted node
    // if deleting the head, need to update the new root
    if(index == 0)
    {
        DNode<T>* new_root = root->get_next();    // the second element will be the new root
        new_root->set_prev(nullptr);    // set the prev pointer to null since it will be the root
        val = root->get_value();
        delete root;
        root = new_root;
    }
    else
    {
        DNode <T>* node_before_deleted = root;
        for (int i = 0; i < index - 1; i++)
        {
            node_before_deleted = node_before_deleted->get_next();
        }
        // after reaching the position of the node deletion, we will start deleting it

        DNode<T>* to_be_deleted = node_before_deleted->get_next();
        to_be_deleted->get_next()->set_prev(node_before_deleted);
        node_before_deleted->set_next(to_be_deleted->get_next());
        val = to_be_deleted->get_value();
        std::cout << val << std::endl;
        delete to_be_deleted;
    }
    // update length
    length--;

    return val;
}

//Size
template <typename T>
int DoublyLinkedList<T>::Size() {
    return length; //get the length of the list.
}

//Print all of the elements of the list
template <typename T>
void DoublyLinkedList<T>::Print() {
    DNode <T>* it = root;

    //if root is nullptr, return error case.
    if (it == nullptr) { std::cout << "there is no element!" << std::endl; }
    else { // return all elements of list.
        std::cout << "(";
        while (it != nullptr) {
            std::cout << it->get_value() << " "; //get the current element.
            it = it->get_next(); //move to the next.
        }
        std::cout << ")" << std::endl;
    }
}

template<typename T>
void DoublyLinkedList<T>::ReversePrint()
{
    DNode<T>* tail = root;
    for (int i = 0; i < length-1; i++)
    {
        tail = tail->get_next();
    }

    for (int i = 0; i < length; i++)
    {
        std::cout << tail->get_value() << "  ";
        tail = tail->get_prev();
    }
    std::cout << std::endl;
}
