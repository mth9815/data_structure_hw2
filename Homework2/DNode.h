//Author: Sijia Zhang, Ziyi Wang
//create time: 10/29/2017

#ifndef D_Node_H
#define D_Node_H

template<typename T>
class DNode{
private:
    DNode<T>* prev;
    DNode<T>* next;
    T val;
public:
    //default constructor.
    DNode() = default;

    // constructor using val
    DNode(int val_) :val(val_), prev(nullptr), next(nullptr)
    {}

    //default destructor.
    ~DNode() = default;

    // get value of current node.
    T get_value() {
        return val;
    }

    // get pointer to next node.
    DNode<T>* get_next() {
        return next;
    }

    // get pointer to previous node.
    DNode<T>* get_prev()
    {
        return prev;
    }

    // set the original value to a new value.
    void set_value(T value_new) {
        val = value_new;
    }

    // set the original pointer to a new pointer to next.
    void set_next(DNode<T>* n) {
        next = n;
    }

    // set the previous pointer to a new pointer to next.
    void set_prev(DNode<T>* n)
    {
        prev = n;
    }
};
#endif
