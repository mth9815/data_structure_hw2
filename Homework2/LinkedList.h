//Author: Sijia Zhang, Ziyi Wang
//create time: 10/29/2017

#ifndef LinkedList_HPP
#define LinkedList_HPP

#include <iostream>
#include "ListIterator.h"

//This class is for giving the LinkedList class some functionality.
template <typename T>
class LinkedList{
private:
    // we need to get the root of the Node and also initial length of the linked list.
    Node <T>* root;
    int length;
public:
    //constructor.
    LinkedList():root(nullptr), length(0)
    {}

    //destructor.
    ~LinkedList();

    //Add the specific element at the end of the list
    void Add(T value);

    //Insert an element at the specific index
    void Insert(T value, int index);

    //Get the element at the specific index.
    T Get(int index);

    //Retrieve the index of the specific element (-1 if it doesn't exist in the list).
    int IndexOf(T value);

    //Remove the element at the specific index and return it
    T Remove(int index);

    //Return an iterator on this list
    ListIterator<T> GetIterator();

    //Return the size of the list.
    int Size();

    //Print function
    void Print();
};


template<typename T>
LinkedList<T>::~LinkedList() {
    Node<T>* curr = root;
    for (int i = 0; i < length; i++)
    {
        Node<T>* next = curr->get_next();
        //std::cout << "deleting" << curr->get_value() << std::endl;
        delete curr;
        curr = next;
    }
}

//Add
template <typename T>
void LinkedList<T>::Add(T val) {
    //create a place for the new node.
    Node <T>* Node_new = new Node<T>(val);

    //cout << std::get<0>(val) << endl;
    //If the root is NULL, then directly set Node_new as root.
    if(root == nullptr){
        this->root = Node_new;
    }
        //else, connect Node_new to the end of original linked list.
    else{
        Node <T>* it = root;
        while(it->get_next() != nullptr){
            it = it->get_next(); //until the end of the list.
        }
        it->set_next(Node_new);
    }
    //add 1 to length.
    length+=1;
}

//Insert
template <typename T>
void LinkedList<T>::Insert(T val, int index) {
    //create a place for the new Node.
    Node <T>* new_node = new Node<T>(val);

    //If the index is larger than length, then error occurs.
    if(index > length || index < 0){
        std::cout<<"out of scope."<<std::endl;
        return;
    }

    if (index == 0)
    {
        new_node->set_next(root);
        root = new_node;
    }
    else
    {
        Node<T>* curr = root;
        for (int i = 0; i < index-1; i++)
        {
            curr = curr->get_next();
        }
        new_node->set_next(curr->get_next());
        curr->set_next(new_node);
    }
    // update length
    length++;
}

//Get
template <typename T>
T LinkedList<T>::Get(int index) {
    //If index is larger than length, then error occurs.
    if(index >= length || index < 0){
        std::cout<<"out of scope!"<<std::endl;
        return T();
    }
    Node <T>* it = root;
    for (int i = 0; i < index; i++)
    {
        it = it->get_next();
    }
    //return the value of given spot.
    return it->get_value();
}

//IndexOf
template <typename T>
int LinkedList<T>::IndexOf(T val) {
    //if this is an empty list, return -1 to show doesn't find
    if(root == nullptr){
        return -1;
    }
    Node <T>* it = root;
    for (int i = 0; i < length; i++)
    {
        //std::cout << std::get<0>(it->get_value()) << std::endl;
        if (it->get_value() == val)
        {
            return i;
        }
        it = it->get_next();
    }
    return -1;
}

//Remove
template <typename T>
T LinkedList<T>::Remove(int index) {
    // check index validation
    if (index < 0 || index >= length)
    {
        std::cout << "index out of range for remove" << std::endl;
        return T();
    }

    T val; // value of the deleted node
    // if deleting the head, need to update the new root
    if (index == 0)
    {
        Node<T>* new_root = root->get_next();
        val = root->get_value();
        delete root;
        root = new_root;
    }
    else
    {
        Node <T>* node_before_deleted = root;
        for (int i = 0; i < index - 1; i++)
        {
            node_before_deleted = node_before_deleted->get_next();
        }
        // after reaching the position of the node deletion, we will start deleting it
        Node<T>* to_be_deleted = node_before_deleted->get_next();
        node_before_deleted->set_next(to_be_deleted->get_next());
        val = to_be_deleted->get_value();
        delete to_be_deleted;
    }
    // update length
    length--;

    return val;
}

// return the iterator, first pointing to the root
template <typename T>
ListIterator<T> LinkedList<T>::GetIterator() {
    return ListIterator<T>(root);
}

//Size
template <typename T>
int LinkedList<T>::Size() {
    return length; //get the length of the list.
}

//Print all of the elements of the list
template <typename T>
void LinkedList<T>::Print() {
    Node <T>* it = root;

    //if root is nullptr, return error case.
    if (it == nullptr) { std::cout << "there is no element!" << std::endl; }
    else { // return all elements of list.
        std::cout << "(";
        while (it != nullptr) {
            std::cout << it->get_value() << " "; //get the current element.
            it = it->get_next(); //move to the next.
        }
        std::cout << ")" << std::endl;
    }
}
#endif //LinkedList_HPP


