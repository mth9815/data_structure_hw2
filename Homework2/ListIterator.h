//Author: Sijia Zhang, Ziyi Wang
//create time: 10/29/2017

#ifndef ListIterator_HPP
#define ListIterator_HPP

#include <iostream>
#include "Node.h"
#include "DNode.h"
//This is the class to realize the iterator in the single linked list.
template <typename T>
class ListIterator{
private:
    //create an iterator as a member.
    Node<T>* iterator;
    DNode<T>* double_iterator;
public:
    //default constuctor.
    ListIterator()=default;

    // constructor for the list iterator
    ListIterator(Node<T>* it):iterator(it)
    {}

    // constructor for the doule list iterator
    ListIterator(DNode<T>* it) :double_iterator(it)
    {}

    //default destructor.
    ~ListIterator()=default;

    // return whether this is a nullptr
    bool IsNull()
    {
        return iterator == nullptr;
    }

    void GoToNext()
    {
        iterator = iterator->get_next();
    }

    //return the next element in this iterator.
    T Next() {
        if (HasNext())
            return iterator->get_next()->get_value();
        else{
            std::cout << "already at the end" << std::endl;
            return T();    // return an empty object as a place holder
        }
    }
    //return whether there is another element to return in this iterator.
    bool HasNext() {
        if (iterator->get_next() == nullptr)
            return false;
        else
            return true;
    }

    T GetVal(){
        return iterator->get_value();
    }

    T DoubleIteratorGetVal(){
        return double_iterator->get_value();
    }

    bool DoubleIteratorHasNext() {
        if (double_iterator->get_next() == nullptr)
            return false;
        else
            return true;
    }

    T DoubleIteratorNext() {
        if (DoubleIteratorHasNext())
            return double_iterator->get_next()->get_value();
        else {
            std::cout << "reach the end of list" << std::endl;
            return T();
        }
    }
};
#endif //ListIterator_HPP
