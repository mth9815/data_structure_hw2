//Author: Ziyi Wang, Sijia Zhang
//create time: 10/29/2017

#ifndef Hasher_HPP
#define Hasher_HPP

//Hasher is a abstract base class to provide concrete implementations of a hashing function.
template <typename K>
class Hasher{
public:
    virtual int hash_function(K key)=0; //a pure virtual function to be overridened in derived class.
};

template<typename K>
class StringHasher: public Hasher<K>{
public:
    int hash_function(K key)
    {
        return (key[0] - 'A');
    }
};

#endif //Hasher_HPP