//Author: Sijia Zhang, Ziyi Wang
//create time: 10/29/2017

#ifndef Node_HPP
#define Node_HPP

#include <iostream>

//This class is used to define node for a single linked list.
template <typename T>
class Node{
private:
    //a single lined list should contain 2 parts: value of each node and pointer to next node.
    T value; //value of the node
    Node <T>* next; //pointer to next node.
public:
    //default constructor.
    Node()=default;

    // constructor using val
    Node(T val_):value(val_), next(nullptr)
    {}

    //default destructor.
    ~Node()=default;

    //we define get and set function in order to be used in LinkedList class.
    //get
    T get_value(){
        return value; // get value of current node.
    }

    Node <T>* get_next(){
        return next; // get pointer to next node.
    }

    //set
    void set_value(T value_new){
        value=value_new; // set the original value to a new value.
    }

    void set_next(Node <T>* next_new){
        next=next_new; // set the original pointer to a new pointer to next.
    }
};

#endif //Node_HPP
