//Author: Ziyi Wang, Sijia Zhang
//create time: 10/29/2017
#ifndef HashTable_CPP
#define HashTable_CPP

#include "HashTable.h"
#include "Hasher.h"
#include "EqualityPredicate.h"

//destructor
template<typename K, typename V>
HashTable<K,V>::~HashTable()
{
    cout << "destructor" << endl;
    for (int i = 0; i < t_size; i++)
        delete hash_table[i];
}


template<typename K, typename V>
// Check whether a given key exists
// return a two-element tuple,
// the first one indicates whether it is found,the second element indicates the value
// If the first element is false, the second value is meaningless
tuple<bool, V> HashTable<K, V>::Find(K key)
{
    int pos = hasher->hash_function(key);
    LinkedList<tuple<K, V>>* list = hash_table[pos];
    ListIterator<tuple<K, V>> it = list->GetIterator();

    while (!it.IsNull())
    {
        auto val = it.GetVal();
        if (eq_p->equal(std::get<0>(val), key))        // use equality predicate to see whether keys are equal
            return make_tuple(true, std::get<1>(val));
        it.GoToNext();    // update the iterator
    }
    return make_tuple(false, V());
}


// Delete the element with specific key
// return false if the key to be deleted doesn't exist in the hash_table
// return true if the delete successed
template<typename K, typename V>
bool HashTable<K, V>::Delete(K key)
{
    bool has_key = std::get<0>(Find(key));
    V val = std::get<1>(Find(key));
    if (!has_key)
        return false;
    else
    {
        int pos = hasher->hash_function(key);
        LinkedList<tuple<K, V>>* list = hash_table[pos];
        int index = list->IndexOf(make_tuple(key, val));
        list->Remove(index);
        return true;
    }
}


// add an element, if already exist, return false
// return true if the add successed
template<typename K, typename V>
bool HashTable<K, V>::Add(K key, V val)
{
    int pos = hasher->hash_function(key);
    LinkedList<tuple<K, V>>* list = hash_table[pos];
    if (list->IndexOf(make_tuple(key, val)) == -1)
    {
        list->Add(make_tuple(key, val));
        return true;
    }
    else
    {
        cout << "already exist" << endl;
        return false;
    }
}


// modify the value associated with the key using the new value
// return true if the modification succeeded
// return false if the hash_table doesn't contain the key
template<typename K, typename V>
bool HashTable<K, V>::Modify(K key, V new_val)
{
    bool has_key = std::get<0>(Find(key));
    V val = std::get<1>(Find(key));    // get the value associated with this key
    if (!has_key)
        return false;
    else
    {
        int pos = hasher->hash_function(key);
        LinkedList<tuple<K, V>>* list = hash_table[pos];
        int index = list->IndexOf(make_tuple(key, val));
        list->Remove(index);
        list->Add(make_tuple(key, new_val));
        return true;
    }
}

#endif
