//Author: Ziyi Wang, Sijia Zhang
//create time: 10/29/2017

#ifndef HashTable_HPP
#define HashTable_HPP

#include "LinkedList.h"
#include "Hasher.h"
#include "EqualityPredicate.h"
#include "ListIterator.h"
#include <tuple>
#include <vector>

using std::tuple;
using std::cout;
using std::endl;
using std::make_tuple;

template <typename K, typename V>
class HashTable{
public:
    std::vector<LinkedList<tuple<K,V>>*> hash_table;
    int t_size;                  //size of the array in the hash table
    Hasher<K>* hasher;           //map a key type to a int
    EqualityPredicate<K>* eq_p; //check whether two keys are equal

public:
    //constructor
    HashTable(Hasher<K>* hasher_, EqualityPredicate<K>* eq_p_) : hasher(hasher_), eq_p(eq_p_)
    {
        t_size = 100;
        // initialize the hash table
        for (int i = 0; i < t_size; i++)
        {
            hash_table.push_back(new LinkedList<tuple<K, V>>());
        }
    }

    // destructor
    ~HashTable();

    // Check whether a given key exists
    // return a two-element tuple,
    // the first one indicates whether it is found,the second element indicates the value
    // If the first element is false, the second value is meaningless
    tuple<bool, V> Find(K key);

    // Delete the element with specific key
    // return false if the key to be deleted doesn't exist in the hash_table
    // return true if the delete successed
    bool Delete(K key);

    // add an element, if already exist, return false
    // return true if the add successed
    bool Add(K key, V val);

    // modify the value associated with the key using the new value
    // return true if the modification succeeded
    // return false if the hash_table doesn't contain the key
    bool Modify(K key, V new_val);
};

#include "HashTable.cpp"
#endif //HashTable_HPP
