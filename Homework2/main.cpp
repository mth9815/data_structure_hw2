#include <iostream>
#include "LinkedList.h"
#include "DoubleLinkedList.cpp"

#include "HashTable.h"
#include "Hasher.h"
#include "EqualityPredicate.h"

#include<string>
using std::string;
using std::cout;
using std::endl;

void Test()
{
    cout << "***************************************************" << endl;
    cout <<  "**********Testing Singly Linked List**************" << endl;
    //test the functions in Single Linked List.
    //test Node.
    Node<double> *n1 = new Node<double>();

    //set value
    n1->set_value(1.5);

    //get value
    std::cout << "get n1 current value:" << '\t' << n1->get_value() << std::endl;
    std::cout << std::endl;

    //set next
    Node<double> *n2 = new Node<double>();
    n2->set_value(5.1);
    n1->set_next(n2);

    //get next and print the value
    std::cout << "After combining n2 to the end of n1, the value of the next part is:" << '\t' << n1->get_next()->get_value() << std::endl;
    std::cout << '\n' << std::endl;


    //test LinkedList
    std::cout << "******Testing LinkedList*****" << std::endl;
    LinkedList<double> l1;

    //Add
    l1.Add(1.1);
    l1.Add(2.2);
    l1.Add(3.3);
    //print
    std::cout << "Add 1.1, 2.2 and 3.3 to the list 1, then we have:" << '\t';
    l1.Print();
    std::cout << std::endl;

    //Insert
    l1.Insert(0.555, 1);
    std::cout << "insert 0.555 to the first place, then we have:" << '\t';
    l1.Print();
    std::cout << std::endl;

    //*************************************************************************error
    //Get
    std::cout << "Get the second element from l1, that is:" << '\t' << l1.Get(1) << std::endl;
    std::cout << std::endl;
    //i*****************************************

    //IndexOf
    std::cout << "Retreive the index of 3.3, the index is:" << '\t' << l1.IndexOf(3.3) << std::endl;
    std::cout << std::endl;

    //Remove
    std::cout << "Remove the third element from l1, the element is:" << '\t' << l1.Remove(2) << std::endl;
    std::cout << "After removing, l1 becomes:";
    l1.Print();
    std::cout << std::endl;

    //Iterator
    std::cout << "Test iterator, (HasNext) will return the second element in the interator, that value is:" << '\t';
    ListIterator<double> it = l1.GetIterator();
    //test HasNext in class ListIterator.
    if (it.HasNext()) {
        //std::cout << it.Next()->get_value() << std::endl; //if it has next node, then will return the second element in the list.
    }
    std::cout << std::endl;

    //Size
    std::cout << "Return the current size of list";
    l1.Print();
    std::cout << "Size is" << '\t' << l1.Size() << std::endl;

    //test ListIterator
    std::cout << "******Testing ListIterator*****" << std::endl;
    ListIterator<double> li = l1.GetIterator();

    //Next
    //std::cout << "let li be the iterator if l1, then the second element contains value:" << li.Next()->get_value() << std::endl;
    cout << endl;
}

void TestInsert()
{
    LinkedList<int> lst;
    lst.Insert(5, 0);
    lst.Insert(6, 1);
    lst.Insert(7, 2);
    lst.Insert(100, 1);
    lst.Insert(1000, 0);
    lst.Insert(666, 5);
    lst.Print();
}

void TestDoublyList()
{
    cout << "***************************************************" << endl;
    cout << "**********Testing Doubly Linked List**************" << endl;
    DoublyLinkedList<int> d_lst;

    // test add
    for (int i = 0; i < 8; i++)
        d_lst.Add(i);

    // test insert
    d_lst.Insert(10, 1);
    d_lst.Insert(100, 0);
    d_lst.Print();
    d_lst.ReversePrint();

    // test delete
    d_lst.Remove(2);
    d_lst.Remove(2);
    d_lst.Print();
    d_lst.ReversePrint();


    // test iterator
    // need to improve
    auto it = d_lst.GetIterator();
    while (true)
    {
        int val = it.DoubleIteratorNext();
        if (val == 0)
            break;
        else
            std::cout << "element is" << val << std::endl;
    }
    cout << endl;
}

void TestHash()
{
    cout << "***************************************************" << endl;
    cout << "**********Testing Hash Table **************" << endl;

    HashTable <string, int> hashtable(new StringHasher<string>, new StringEqualityPredicate<string>);

    //test add function
    hashtable.Add("Andy", 100);
    hashtable.Add("Alex", 150);

    //test find to see whether we can find Alex that we added
    auto res = hashtable.Find("Alex");
    cout << "found?" << std::get<0>(res) << "   val: " << std::get<1>(res) << endl;

    // modify the element and output the new element
    hashtable.Modify("Alex", 200);
    res = hashtable.Find("Alex");
    cout << "found?" << std::get<0>(res) << "   val: " << std::get<1>(res) << endl;

    // Test delete of the element
    hashtable.Delete("Alex");
    res = hashtable.Find("Alex");
    cout << "After delete, can we found?" << std::get<0>(res) << "   val: " << std::get<1>(res) << endl;
}

int main() {
    Test();
    TestInsert();
    TestDoublyList();
    TestHash();
    return 0;
}