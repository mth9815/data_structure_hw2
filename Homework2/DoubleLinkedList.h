//Author: Sijia Zhang, Ziyi Wang
//create time: 10/29/2017

#ifndef Doubly_Linked_List_HPP
#define Doubly_Linked_List_HPP

#include "DNode.h"
#include "ListIterator.h"
#include <iostream>

template <typename T>
class DoublyLinkedList
{
private:
    DNode<T>* root;
    int length;
public:
    //constructor.
    DoublyLinkedList() :root(nullptr), length(0)
    {}

    //destructor.
    ~DoublyLinkedList();

    //Add the specific element at the end of the list
    void Add(T value);

    //Insert an element at the specific index
    void Insert(T value, int index);

    //Get the element at the specific index.
    T Get(int index);

    //Retrieve the index of the specific element (-1 if it doesn't exist in the list).
    int IndexOf(T value);

    //Remove the element at the specific index and return it
    T Remove(int index);

    //Return an iterator on this list
    ListIterator<T> GetIterator();

    //Return the size of the list.
    int Size();

    //Print the value of all nodes
    void Print();

    //Print the value of all nodes in a reverse way
    void ReversePrint();
};
#endif // !Doubly_Linked_List