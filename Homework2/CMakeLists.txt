cmake_minimum_required(VERSION 3.7)
project(test_case)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp DNode.h DoubleLinkedList.h DoubleLinkedList.cpp EqualityPredicate.h Hasher.h HashTable.h LinkedList.h Node.h ListIterator.h HashTable.cpp)
add_executable(test_case ${SOURCE_FILES})