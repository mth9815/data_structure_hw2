//Author: Ziyi Wang, Sijia Zhang
//create time: 10/29/2017

#ifndef EqualityPredicate_HPP
#define EqualityPredicate_HPP

//EqualityPredicate is an abstract class for equality predicate function.
template <typename K>
class EqualityPredicate{
public:
    virtual bool equal(K key1, K key2)=0; //a pure virtual function.
};

template<typename K>
class StringEqualityPredicate :public EqualityPredicate<K> {
public:
    bool equal(K key1, K key2)
    {
        return key1 == key2;
    }
};

#endif //EqualityPredicate_HPP